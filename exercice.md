# Rajouter au projet la gestion des produits
(GetAll + Create)

Un produit aura :
* Id
* Name (requis)
* Description (requis)
* Image (nom fichier)
* (DescriptionAudio) (nom fichier)

## Dans un premier temps, permettre de créer un produit et d'ajouter une image.
Il y a plusieurs façons de le gérer. Je vous conseille de vous renseigner sur la façon d'ajouter un fichier dans votre serveur dans un dossier et de rendre ce dossier accessible. On stock alors en DB le nom du fichier. Pour accéder au fichier : votreurlapi/dossier/nomfichier.ext

## Dans un deuxième temps, si aucune image n'est fournie, on en génère une avec Dall-E
Pour les images :
Travaillez avec dall-e-2 en 256x256 (pour faire payer Khun moins cher pliz 🙏) 

## Dans un troisième (et dernier) temps, rajouter une description audio avec textToSpeech basée sur la description texte du produit