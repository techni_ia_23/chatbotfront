# Install Tailwind :
https://tailwindcss.com/docs/guides/angular
* Installer :
```
    npm install -D tailwindcss postcss autoprefixer
    (installe toutes les lib dont on a besoin)

    npx tailwindcss init
    (créé le fichier de config)
```
* Rajouter dans le fichier tailwind.config
```
    content : [
        "./src/**/*.{html,ts}"
    ]
```
* Dans le fichier style global : styles.scss :
```
    @tailwind base;
    @tailwind components;
    @tailwind utilities;
```
# Extension sympa quand on utilise Tailwind :
Tailwind CSS IntelliSense v0.10.5 - Tailwind Labs
