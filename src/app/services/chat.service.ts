import { Injectable } from '@angular/core';
import { Message } from '../models/Message.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ChatService {

  private _apiUrl : string = 'https://localhost:7245/api/Chat';

  constructor(private _http : HttpClient ) { }

  chatWithBot(messages : Message[]) : Observable<Message[]> {
   
    
      return this._http.post<Message[]>(this._apiUrl, messages);
  }

}
