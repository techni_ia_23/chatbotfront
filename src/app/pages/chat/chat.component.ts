import { Component } from '@angular/core';
import { Message } from '../../models/Message.model';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ChatService } from '../../services/chat.service';
import { HttpClientModule } from '@angular/common/http';
import { finalize } from 'rxjs';

@Component({
  selector: 'app-chat',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, HttpClientModule],
  templateUrl: './chat.component.html',
  styleUrl: './chat.component.scss'
})
export class ChatComponent {

  chatForm: FormGroup;

  messages: Message[] = [
    { role: 'assistant', content: 'Bonjour, je suis Nono, le robot qui a la réponse à toutes vos questions', date: new Date() },
    { role: 'assistant', content: 'En quoi puis-je vous aider ?', date: new Date() },
  ];

  isLoading: boolean = false;

  constructor(private _fb: FormBuilder, private _chatService: ChatService) {
    this.chatForm = this._fb.group({
      'content': [null, Validators.required]
    })
  }

  sendMessage() {
    this.isLoading = true;
    // 1) Créer le message et l'ajouter dans la liste
    const message: Message = {
      role: 'user',
      content: this.chatForm.get('content')?.value,
      date: new Date()
    }
    this.messages.push(message);
    this.chatForm.reset();

    // 2) Envoyer la liste des messages à l'API
    this._chatService.chatWithBot(this.messages)
      .pipe(finalize(() => { this.isLoading = false }))
      .subscribe({
        next: (res) => {
          // 3) Si success : On ajoute la(les) réponse(s) du robot dans la liste
          this.messages.push(...res);
          //this.messages = [...this.messages, ...res];
        },
        error: (error) => {
          // 4) Si erreur : On va devoir retirer le message du user de la liste
          this.messages = this.messages.filter(m => m != message);
        }

      })

  }

}
